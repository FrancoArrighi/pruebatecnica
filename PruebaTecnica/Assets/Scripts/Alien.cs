﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Alien : MonoBehaviour
{

	public Canvas ui;
	public Text txtAlien;
	public Transform[] targets;
	public float speed;

	[SerializeField]
    private int targetNum = 0;
	
	public int cocoon_collected = 0;

    void Awake()
	{
		txtAlien.text = cocoon_collected.ToString();
        Reset();
	}

	public void Reset()
	{
		GetComponent<Animator>().Play("Grounded");
		GetComponent<Animator>().SetFloat("Forward", 1f);
	}

	void Update () {

		if(targets != null)
		{
			var delta = targets[targetNum].position - transform.position;
			delta.y = 0f;
			var deltaLen = delta.magnitude;
			var move = Mathf.Min(speed * Time.deltaTime, deltaLen);
			if (deltaLen <= 5f && targetNum < targets.Length - 1)
                targetNum++;

            else
			{
				var direction = delta / deltaLen;
				transform.forward = Vector3.Slerp(transform.forward, direction, 10f * Time.deltaTime);
				GetComponent<CharacterController>().Move(move * transform.forward + Vector3.down * 3f);
			}
		}
        txtAlien.text = cocoon_collected.ToString();
    }
	public void AddPoints()
	{
		cocoon_collected += 1;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Finish"))
        {
            this.gameObject.SetActive(false);
        }
    }
}
