﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cocoon : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Alien"))
        {
            Director.hits += 1;
            other.GetComponent<Alien>().AddPoints();
        }
    }
}
