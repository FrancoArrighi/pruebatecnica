﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public Transform start;
	public Transform[] targets;
	public Alien prefabAlien; 
	//public Canvas prefabAlienUI;  el alien es el instanciarse con su respectiva ui
	public ConfigPanel config;
	public Director director;

	public float timer;
	private bool canSpawn;

	void ResetAlien(Alien alien)
	{
		alien.speed = config.speed;
        alien.transform.position = start.transform.position;
        alien.transform.rotation = start.transform.rotation;
        alien.Reset();
	}

	Alien CreateAlien()
	{
		var alien = Instantiate(prefabAlien, this.transform);
        alien.targets = targets;
        alien.speed = config.speed;
        ResetAlien(alien);

		return alien;
	}

	void Update()
	{
		timer += Time.deltaTime;

		if(timer >= (float)1 / config.rate) 
		{
            CreateAlien();
			timer = 0;
        }
	}
}
