﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Experimental.PlayerLoop;

public class Director : MonoBehaviour {

	public ConfigPanel config;

	public static float hits;
	public float time;
	public int hitspersecond;

	Alien[] aliens;
	public Transform eje;
	public List<float> angles = new List<float>();
    public float avgAngle = 0f;

    void Update () {
	    aliens = FindObjectsOfType<Alien>();
        int aliensAlive = aliens.Length;

		//Update stats
		config.txtInfo.text =
			$"{aliensAlive} aliens alive"
			+ $"\n{hitspersecond} cocoons hits p/sec"
			+ $"\n{avgAngle:F2}° average angle"
			;

		ChangeAliensSpeed();
		CheckAverageAngle();
		StartCoroutine(CheckHitsPerSecond());
		time += Time.deltaTime;       
    }
	IEnumerator CheckHitsPerSecond()
	{
		yield return new WaitForSeconds(1);
        hitspersecond = (int)(hits / time);
    }

    public void ChangeAliensSpeed()
    {
		foreach (Alien alien in aliens)
		{
			alien.speed = config.speed;
		}
    }
	public float CalculateAngle(Vector3 pos, Vector3 alien) // falta
	{
		Vector3 vector = pos - alien;
	
		float _angle = Mathf.Atan2(vector.y,vector.x) * Mathf.Rad2Deg;
		return _angle;
	}

	public void CheckAverageAngle()
	{
		if (aliens.Length > 0)
		{
			angles.Clear();
			foreach (Alien a in aliens)
			{
				angles.Add(CalculateAngle(transform.position, a.transform.position));
			}		
		}
		if (angles.Count > 0)
		{
			avgAngle = angles.Average();
		}
    }
}
